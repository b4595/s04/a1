<?php

class Building {
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors,$address)
	{
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;

	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getFloor()
	{
		return $this->floors;
	}

	public function setFloor($floors)
	{
		return $floors(8);
	}

	public function getAddress()
	{
		return $this->address;
	}

	public function setAddress($address)
	{
		return $address('Timog Avenue, Quezon City, Philippines');
	}


}

class Condominium extends Building
{
	//Encapsulation - getter
	//get certain value from the property
	public function getName()
	{
		return $this->name;
	}

	//Encapsulation - setter
	//modify the property
	public function setName($name)
	{
		$this->name = $name;
	}

	public function getFloor()
	{
		return $this->floors;
	}

	public function setFloor($floors)
	{
		return $floors(5);
	}

	public function getAddress()
	{
		return $this->address;
	}

	public function setAddress($address)
	{
		return $address('Buendia Avenue, Makati City, Philippines');
	}
}

$building = new Building ('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium
(
	'Enzo Condo', 
	5, 
	'Buendia Avenue, Makati City, Philippines'
);